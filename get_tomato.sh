#!/bin/bash

working_dir=$PWD
cd ~
go get -v github.com/tomatool/tomato
cd $working_dir
make build
./tomatest &
tomatest_pid=$!
tomato -f ./functiontest/ -e ./functiontest/.tomato.env ./functiontest/tomato.yml
kill -9 $tomatest_pid