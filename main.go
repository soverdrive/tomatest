package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

func main() {
	testMux := http.NewServeMux()
	testMux.HandleFunc("/http", testHTTP)
	testMux.HandleFunc("/migrate", testMigration)
	testMux.HandleFunc("/db", testDB)
	testMux.HandleFunc("/api", testAPI)

	var port = "9999"
	log.Printf("listening on port: %s", port)
	http.ListenAndServe(fmt.Sprintf(":%s", port), testMux)
}

func testHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodHead {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(fmt.Sprintf("not accepting %s", r.Method)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}

func testDB(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("not accepting %s", r.Method)))
		return
	}

	var idReq string
	idReq = r.URL.Query().Get("id")

	db, err := sql.Open("postgres", "dbname=test user=postgres sslmode=disable")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("db open error %v", err)))
		return
	}
	defer db.Close()

	res := db.QueryRow("SELECT id, note FROM testtest WHERE id = $1", idReq)
	var (
		idRes   int64
		noteRes string
	)
	err = res.Scan(&idRes, &noteRes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("db open error %v", err)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("id: %d, note: %s", idRes, noteRes)))
}

func testMigration(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("not accepting %s", r.Method)))
		return
	}

	db, err := sql.Open("postgres", "dbname=test user=postgres sslmode=disable")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("db open error %v", err)))
		return
	}
	defer db.Close()

	migrateQuery, err := ioutil.ReadFile("./migrate.sql")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("migrate error %v", err)))
		return
	}

	_, err = db.Exec(string(migrateQuery))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("exec error %v", err)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}

func testAPI(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("not accepting %s", r.Method)))
		return
	}

	resp, err := http.DefaultClient.Get("https://api.github.com/repos/tomatool/tomato/releases/latest")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("http get error %v", err)))
		return
	}

	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("read all error %v", err)))
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("status non-200 %v", resp.StatusCode)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(res)
	return
}
