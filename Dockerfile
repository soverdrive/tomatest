FROM golang:latest

RUN mkdir /app
COPY . /app
WORKDIR /app
RUN go build -o tomatest

EXPOSE 9999/tcp
CMD ["./tomatest"]