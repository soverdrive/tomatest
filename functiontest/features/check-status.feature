Feature: Check my application's status endpoint
  Scenario: My application is running and active
    Given "tomatest" send request to "HEAD /http"
    Then "tomatest" response code should be 200