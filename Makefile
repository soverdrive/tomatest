run:
	@go build && ./tomatest

build:
	@go build -o tomatest

test:
	@go test -v -race ./...
